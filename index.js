const express = require('express')
const fs = require("fs")
const path = require('path');
const app = express()

app.use(express.json({extended: true}))

const logFile = "./requests.log"
const filesDir = "./files"
const allowedExtNames = [
    ".log",
    ".txt",
    ".json",
    ".yaml",
    ".xml",
    ".js"
]

const updateLogs = (message) => {
    const res = JSON.stringify(message)

    fs.appendFile(logFile, `${res}\n`, function (err) {
        if (err) throw err;
    });
}

app.listen(8080, function(){
    console.log("Server listening on port", this.address().port);

});

app.get("/", function (req, res) {
    updateLogs({
        request: `Get base url`,
        date: new Date(),
    })

    res.status(200).json({"status": "OK"})
})

app.get("/api/files/:fileName", function (req, res) {
    try {
        updateLogs({
            request: `Get file by filename: ${req.params.fileName}`,
            date: new Date(),
        })
        fs.readFile(`${filesDir}/${req.params.fileName}`, 'utf8', (err, data) => {
            if (err) {
                res.status(400).json({
                    "message": `No file with '${req.params.fileName}' filename found`
                })
                return
            }

            fs.stat(`${filesDir}/${req.params.fileName}`, (err, stats) => {
                if(err) {
                    res.status(400).json({
                        "message": "Client error"
                    })
                    return
                }

                res.status(200).json({
                    "message": "Success",
                    "filename": req.params.fileName,
                    "content": data,
                    "extension": path.extname(req.params.fileName).slice(1),
                    "uploadedDate": stats.mtime
                })
            });
        })
    } catch (e) {
        res.status(500).json({
            "message": "Server error"
        })
    }
})

app.route("/api/files")
    .get(function (req, res) {
        try {
            updateLogs({
                request: `Get files list`,
                date: new Date(),
            })
            fs.readdir(filesDir, (err, files) => {

                if (err) {
                    res.status(400).json({
                        "message": "Client error"
                    })
                    return
                }

                res.status(200).json({
                    "message": "Success",
                    "files": files
                })
            })
        } catch (e) {
            res.status(500).json({
                "message": "Server error"
            })
        }

    })
    .post(function (req, res) {
        try {
            updateLogs({
                request: `Add new file`,
                date: new Date(),
            })
            const {filename, content} = req.body

            if (!content) {
                res.status(400).json({
                    "message": "Please specify 'content' parameter"
                })
                return
            }

            const isAllowed = allowedExtNames.find(el => el === path.extname(filename))

            if (!!isAllowed) {

                fs.writeFile(`${filesDir}/${filename}`, content, (err, files) => {
                    if (err) {
                        res.status(400).json({
                            "message": "Please specify 'content' parameter"
                        })
                        return
                    }
                    res.status(200).json({
                        "message": "File created successfully"
                    })
                })

            } else {
                res.status(400).json({
                    "message": "Extension is not allowed"
                })
            }
        } catch (e) {
            console.log(e)
            res.status(500).json({
                "message": "Server error"
            })
        }

    })

app.get("*", function (req, res) {
    updateLogs({
        request: `wrong address`,
        date: new Date(),
    })
    res.status(404).json({"error": "page not found"})
})